﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //2
            string small = "segi";
            string capital = small.ToUpper();
            Console.WriteLine(small);
            Console.WriteLine(capital);

            //3
            string greatAgain = "hello ";
            Console.WriteLine(greatAgain);
            Console.ReadLine();

            //4
            string firstName = "Jack ";
            string lastName = "Jame ";
            string name = firstName + lastName;
            string call = greatAgain + firstName + lastName;
            Console.WriteLine(call);
            Console.ReadLine();

            //5
            char charA = 'a';
            char charB = 'b';
            Console.WriteLine(charA.Equals('a'));
            Console.ReadLine();

            //6
            string course;
            Console.WriteLine("Enter your course name: ");
            course = Console.ReadLine();
            Console.WriteLine("Your course is {0}", course);

            //7
            string[] courses = { "Web Services", "Windows Programming" };
            Console.WriteLine(courses[1]);

            //8
            int[] n = new int[10];

            for (int i = 0; i < 10; i++)
            {
                n[i] = i + 100;
            }

            foreach (int j in n)
            {
                int i = j - 100;
                Console.WriteLine("Element Array [{0}] = {1}", i, j);
            }
        }
    }
}
