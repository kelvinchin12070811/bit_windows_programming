﻿namespace Program1
{
    partial class WelcomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbUsrname = new System.Windows.Forms.Label();
            this.imgUsrAvatar = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgUsrAvatar)).BeginInit();
            this.SuspendLayout();
            // 
            // lbUsrname
            // 
            this.lbUsrname.AutoSize = true;
            this.lbUsrname.Location = new System.Drawing.Point(306, 55);
            this.lbUsrname.Name = "lbUsrname";
            this.lbUsrname.Size = new System.Drawing.Size(49, 13);
            this.lbUsrname.TabIndex = 0;
            this.lbUsrname.Text = "Usrname";
            this.lbUsrname.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // imgUsrAvatar
            // 
            this.imgUsrAvatar.Location = new System.Drawing.Point(305, 2);
            this.imgUsrAvatar.Name = "imgUsrAvatar";
            this.imgUsrAvatar.Size = new System.Drawing.Size(50, 50);
            this.imgUsrAvatar.TabIndex = 1;
            this.imgUsrAvatar.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(12, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(362, 33);
            this.label1.TabIndex = 2;
            this.label1.Text = "Wellcome back to SEGian Forum";
            // 
            // WelcomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 249);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgUsrAvatar);
            this.Controls.Add(this.lbUsrname);
            this.Name = "WelcomForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RightToLeftLayout = true;
            this.Text = "SEGian Forum";
            ((System.ComponentModel.ISupportInitialize)(this.imgUsrAvatar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbUsrname;
        private System.Windows.Forms.PictureBox imgUsrAvatar;
        private System.Windows.Forms.Label label1;
    }
}