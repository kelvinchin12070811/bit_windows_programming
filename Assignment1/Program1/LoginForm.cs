﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Program1
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            Database.InitAllDb();

            forumLogo.Image = Image.FromFile("images/forum_logo.png");
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Database.DumpAllDb();
            base.OnFormClosing(e);
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (Database.VerifyUser(inUsrname.Text, inPassword.Text))
            {
                WelcomForm welcomForm = new WelcomForm();
                welcomForm.Show(this, inUsrname.Text);
            }
            else
            {
                WrongForm totallyWrong = new WrongForm();
                totallyWrong.Show(this);
            }
        }

        private void BtnRegister_Click(object sender, EventArgs e)
        {
            RegisterForm regForm = new RegisterForm();
            regForm.Show(this);
        }
    }
}
