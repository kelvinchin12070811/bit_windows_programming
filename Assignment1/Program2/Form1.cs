﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Program2
{
    public partial class Form1 : Form
    {
        private Image imgCircle = null;
        private Image imgOval = null;
        private Image imgSquare = null;

        public Form1()
        {
            InitializeComponent();
            imgCircle = Image.FromFile("images/circle.jpg");
            imgOval = Image.FromFile("images/oval.jpg");
            imgSquare = Image.FromFile("images/square.jpg");
        }

        private void RbCircle_CheckedChanged(object sender, EventArgs e)
        {
            shapeViewer.Image = imgCircle;
        }

        private void RbOval_CheckedChanged(object sender, EventArgs e)
        {
            shapeViewer.Image = imgOval;
        }

        private void RbSquare_CheckedChanged(object sender, EventArgs e)
        {
            shapeViewer.Image = imgSquare;
        }
    }
}
