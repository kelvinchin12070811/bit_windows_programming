﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program3
{
    public partial class Form1 : Form
    {
        private float markAvg = 0;
        private float markTotal = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            stdnAvg.Text = "";
            stdnGrade.Text = "";
            stdnId.Text = "";
            stdnName.Text = "";
            stdnTotal.Text = "";

            subject1.Text = "";
            subject2.Text = "";
            subject3.Text = "";
        }

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            CalculateMarks();
            IdentifyGrade();
        }

        private void CalculateMarks()
        {
            if (subject1.Text == "" || subject2.Text == "" || subject3.Text == "")
            {
                MessageBox.Show(this, "Please fill in all 3 of the subjects' marks");
                return;
            }

            float mark1 = float.Parse(subject1.Text);
            float mark2 = float.Parse(subject2.Text);
            float mark3 = float.Parse(subject3.Text);

            markTotal = mark1 + mark2 + mark3;
            markAvg = markTotal / 3;

            stdnTotal.Text = markTotal.ToString("0.00");
            stdnAvg.Text = markAvg.ToString("0.00");

            return;
        }

        private void IdentifyGrade()
        {
            string grade = null;
            if (markAvg >= 85) grade = "A+";
            else if (markAvg >= 75) grade = "A";
            else if (markAvg >= 70) grade = "B+";
            else if (markAvg >= 65) grade = "B";
            else if (markAvg >= 60) grade = "C+";
            else if (markAvg >= 55) grade = "C";
            else if (markAvg >= 50) grade = "D";

            if (markTotal < 40) grade = "Fail";

            stdnGrade.Text = grade;
        }
    }
}
