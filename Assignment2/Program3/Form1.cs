﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            listLeft.Items.AddRange(new string[]{
                "Apple",
                "Banana",
                "Mohanan R",
                "Ryzen",
                "Core",
                "Orange"
            });
        }

        private void BtnLToR_Click(object sender, EventArgs e)
        {
            int target = listLeft.SelectedIndex;
            if (target < 0) return;

            string itm = listLeft.Items[target].ToString();
            listLeft.Items.RemoveAt(target);
            listRight.Items.Add(itm);
        }

        private void BtnLToRAll_Click(object sender, EventArgs e)
        {
            foreach (string itr in listLeft.Items)
                listRight.Items.Add(itr);

            listLeft.Items.Clear();
        }

        private void BtnRToL_Click(object sender, EventArgs e)
        {
            int target = listRight.SelectedIndex;
            if (target < 0) return;

            string itm = listRight.Items[target].ToString();
            listRight.Items.RemoveAt(target);
            listLeft.Items.Add(itm);
        }

        private void BtnRToLAll_Click(object sender, EventArgs e)
        {
            foreach (string itr in listRight.Items)
                listLeft.Items.Add(itr);

            listRight.Items.Clear();
        }
    }
}
