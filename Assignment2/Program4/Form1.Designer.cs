﻿namespace Program4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.input = new System.Windows.Forms.TextBox();
            this.ckbBold = new System.Windows.Forms.CheckBox();
            this.ckbItalic = new System.Windows.Forms.CheckBox();
            this.ckbUnderline = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sz12 = new System.Windows.Forms.RadioButton();
            this.sz20 = new System.Windows.Forms.RadioButton();
            this.sz30 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.colBlue = new System.Windows.Forms.RadioButton();
            this.colGreen = new System.Windows.Forms.RadioButton();
            this.colRed = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(202, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "Font Changing";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // input
            // 
            this.input.Location = new System.Drawing.Point(112, 73);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(419, 26);
            this.input.TabIndex = 2;
            // 
            // ckbBold
            // 
            this.ckbBold.AutoSize = true;
            this.ckbBold.Location = new System.Drawing.Point(47, 223);
            this.ckbBold.Name = "ckbBold";
            this.ckbBold.Size = new System.Drawing.Size(67, 24);
            this.ckbBold.TabIndex = 3;
            this.ckbBold.Text = "Bold";
            this.ckbBold.UseVisualStyleBackColor = true;
            this.ckbBold.CheckedChanged += new System.EventHandler(this.CkbBold_CheckedChanged);
            // 
            // ckbItalic
            // 
            this.ckbItalic.AutoSize = true;
            this.ckbItalic.Location = new System.Drawing.Point(47, 262);
            this.ckbItalic.Name = "ckbItalic";
            this.ckbItalic.Size = new System.Drawing.Size(68, 24);
            this.ckbItalic.TabIndex = 4;
            this.ckbItalic.Text = "Italic";
            this.ckbItalic.UseVisualStyleBackColor = true;
            this.ckbItalic.CheckedChanged += new System.EventHandler(this.CkbItalic_CheckedChanged);
            // 
            // ckbUnderline
            // 
            this.ckbUnderline.AutoSize = true;
            this.ckbUnderline.Location = new System.Drawing.Point(47, 296);
            this.ckbUnderline.Name = "ckbUnderline";
            this.ckbUnderline.Size = new System.Drawing.Size(103, 24);
            this.ckbUnderline.TabIndex = 5;
            this.ckbUnderline.Text = "Underline";
            this.ckbUnderline.UseVisualStyleBackColor = true;
            this.ckbUnderline.CheckedChanged += new System.EventHandler(this.CkbUnderline_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sz30);
            this.groupBox1.Controls.Add(this.sz20);
            this.groupBox1.Controls.Add(this.sz12);
            this.groupBox1.Location = new System.Drawing.Point(232, 202);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(123, 130);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Size";
            // 
            // sz12
            // 
            this.sz12.AutoSize = true;
            this.sz12.Location = new System.Drawing.Point(24, 25);
            this.sz12.Name = "sz12";
            this.sz12.Size = new System.Drawing.Size(52, 24);
            this.sz12.TabIndex = 0;
            this.sz12.TabStop = true;
            this.sz12.Text = "12";
            this.sz12.UseVisualStyleBackColor = true;
            this.sz12.CheckedChanged += new System.EventHandler(this.Sz12_CheckedChanged);
            // 
            // sz20
            // 
            this.sz20.AutoSize = true;
            this.sz20.Location = new System.Drawing.Point(24, 55);
            this.sz20.Name = "sz20";
            this.sz20.Size = new System.Drawing.Size(52, 24);
            this.sz20.TabIndex = 1;
            this.sz20.TabStop = true;
            this.sz20.Text = "20";
            this.sz20.UseVisualStyleBackColor = true;
            this.sz20.CheckedChanged += new System.EventHandler(this.Sz20_CheckedChanged);
            // 
            // sz30
            // 
            this.sz30.AutoSize = true;
            this.sz30.Location = new System.Drawing.Point(24, 85);
            this.sz30.Name = "sz30";
            this.sz30.Size = new System.Drawing.Size(52, 24);
            this.sz30.TabIndex = 2;
            this.sz30.TabStop = true;
            this.sz30.Text = "30";
            this.sz30.UseVisualStyleBackColor = true;
            this.sz30.CheckedChanged += new System.EventHandler(this.Sz30_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.colBlue);
            this.groupBox2.Controls.Add(this.colGreen);
            this.groupBox2.Controls.Add(this.colRed);
            this.groupBox2.Location = new System.Drawing.Point(393, 202);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(123, 130);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Colour";
            // 
            // colBlue
            // 
            this.colBlue.AutoSize = true;
            this.colBlue.Location = new System.Drawing.Point(24, 85);
            this.colBlue.Name = "colBlue";
            this.colBlue.Size = new System.Drawing.Size(66, 24);
            this.colBlue.TabIndex = 2;
            this.colBlue.TabStop = true;
            this.colBlue.Text = "Blue";
            this.colBlue.UseVisualStyleBackColor = true;
            this.colBlue.CheckedChanged += new System.EventHandler(this.ColBlue_CheckedChanged);
            // 
            // colGreen
            // 
            this.colGreen.AutoSize = true;
            this.colGreen.Location = new System.Drawing.Point(24, 55);
            this.colGreen.Name = "colGreen";
            this.colGreen.Size = new System.Drawing.Size(79, 24);
            this.colGreen.TabIndex = 1;
            this.colGreen.TabStop = true;
            this.colGreen.Text = "Green";
            this.colGreen.UseVisualStyleBackColor = true;
            this.colGreen.CheckedChanged += new System.EventHandler(this.ColGreen_CheckedChanged);
            // 
            // colRed
            // 
            this.colRed.AutoSize = true;
            this.colRed.Location = new System.Drawing.Point(24, 26);
            this.colRed.Name = "colRed";
            this.colRed.Size = new System.Drawing.Size(64, 24);
            this.colRed.TabIndex = 0;
            this.colRed.TabStop = true;
            this.colRed.Text = "Red";
            this.colRed.UseVisualStyleBackColor = true;
            this.colRed.CheckedChanged += new System.EventHandler(this.ColRed_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(572, 359);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ckbUnderline);
            this.Controls.Add(this.ckbItalic);
            this.Controls.Add(this.ckbBold);
            this.Controls.Add(this.input);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.CheckBox ckbBold;
        private System.Windows.Forms.CheckBox ckbItalic;
        private System.Windows.Forms.CheckBox ckbUnderline;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton sz30;
        private System.Windows.Forms.RadioButton sz20;
        private System.Windows.Forms.RadioButton sz12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton colBlue;
        private System.Windows.Forms.RadioButton colGreen;
        private System.Windows.Forms.RadioButton colRed;
    }
}

