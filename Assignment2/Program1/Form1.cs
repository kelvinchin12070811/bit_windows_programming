﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnUpper_Click(object sender, EventArgs e)
        {
            txbUpper.Text = txbInput.Text.ToUpper();
        }

        private void BtnLower_Click(object sender, EventArgs e)
        {
            txbLower.Text = txbInput.Text.ToLower();
        }

        private void BtnCount_Click(object sender, EventArgs e)
        {
            txbCount.Text = txbInput.Text.Length.ToString();
        }

        private void BtnCopy_Click(object sender, EventArgs e)
        {
            txbCopy.Text = txbInput.Text;
        }

        private void BtnConcat_Click(object sender, EventArgs e)
        {
            txbConcat.Text = txbUpper.Text + txbLower.Text;
        }

        private void BtnCompare_Click(object sender, EventArgs e)
        {
            if (txbInput.Text == txbUpper.Text)
                txbCompare.Text = "Equal";
            else
                txbCompare.Text = "Not equal";
        }

        private void BtnLeftPadding_Click(object sender, EventArgs e)
        {
            if (txbPadding.Text.Length <= 0) return;

            string padding = "";
            for (int itr = 0; itr < 20; itr++)
                padding += " ";

            txbPadding.Text = padding + txbPadding.Text;
        }

        private void BtnPaddingSpace_Click(object sender, EventArgs e)
        {
            string padding = "";
            for (int itr = 0; itr < 20; itr++)
                padding += "*";

            txbLeftStr.Text = padding + txbLeftStr.Text;
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            txbInput.ResetText();
            txbUpper.ResetText();
            txbLower.ResetText();
            txbCount.ResetText();
            txbCopy.ResetText();
            txbConcat.ResetText();
            txbCompare.ResetText();
            txbPadding.ResetText();
            txbLeftStr.ResetText();
        }
    }
}
