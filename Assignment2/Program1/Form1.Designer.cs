﻿namespace Program1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txbInput = new System.Windows.Forms.TextBox();
            this.txbUpper = new System.Windows.Forms.TextBox();
            this.txbLower = new System.Windows.Forms.TextBox();
            this.txbCount = new System.Windows.Forms.TextBox();
            this.txbCopy = new System.Windows.Forms.TextBox();
            this.txbConcat = new System.Windows.Forms.TextBox();
            this.txbCompare = new System.Windows.Forms.TextBox();
            this.txbPadding = new System.Windows.Forms.TextBox();
            this.txbLeftStr = new System.Windows.Forms.TextBox();
            this.btnUpper = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnLeftPadding = new System.Windows.Forms.Button();
            this.btnPaddingSpace = new System.Windows.Forms.Button();
            this.btnConcat = new System.Windows.Forms.Button();
            this.btnLower = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnCompare = new System.Windows.Forms.Button();
            this.btnCount = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(215, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Program For Strings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(12, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enter text:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(12, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Upper:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(12, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Lower:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(12, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Count:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(13, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Copy:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(13, 267);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Concat:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(13, 306);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Compare:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(13, 343);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Left padding:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(12, 382);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "*Left string:";
            // 
            // txbInput
            // 
            this.txbInput.Location = new System.Drawing.Point(132, 72);
            this.txbInput.Name = "txbInput";
            this.txbInput.Size = new System.Drawing.Size(279, 26);
            this.txbInput.TabIndex = 10;
            // 
            // txbUpper
            // 
            this.txbUpper.Location = new System.Drawing.Point(132, 109);
            this.txbUpper.Name = "txbUpper";
            this.txbUpper.Size = new System.Drawing.Size(279, 26);
            this.txbUpper.TabIndex = 11;
            // 
            // txbLower
            // 
            this.txbLower.Location = new System.Drawing.Point(132, 148);
            this.txbLower.Name = "txbLower";
            this.txbLower.Size = new System.Drawing.Size(279, 26);
            this.txbLower.TabIndex = 12;
            // 
            // txbCount
            // 
            this.txbCount.Location = new System.Drawing.Point(132, 186);
            this.txbCount.Name = "txbCount";
            this.txbCount.Size = new System.Drawing.Size(279, 26);
            this.txbCount.TabIndex = 13;
            // 
            // txbCopy
            // 
            this.txbCopy.Location = new System.Drawing.Point(132, 225);
            this.txbCopy.Name = "txbCopy";
            this.txbCopy.Size = new System.Drawing.Size(279, 26);
            this.txbCopy.TabIndex = 14;
            // 
            // txbConcat
            // 
            this.txbConcat.Location = new System.Drawing.Point(132, 264);
            this.txbConcat.Name = "txbConcat";
            this.txbConcat.Size = new System.Drawing.Size(279, 26);
            this.txbConcat.TabIndex = 15;
            // 
            // txbCompare
            // 
            this.txbCompare.Location = new System.Drawing.Point(132, 303);
            this.txbCompare.Name = "txbCompare";
            this.txbCompare.Size = new System.Drawing.Size(279, 26);
            this.txbCompare.TabIndex = 16;
            // 
            // txbPadding
            // 
            this.txbPadding.Location = new System.Drawing.Point(132, 340);
            this.txbPadding.Name = "txbPadding";
            this.txbPadding.Size = new System.Drawing.Size(279, 26);
            this.txbPadding.TabIndex = 17;
            // 
            // txbLeftStr
            // 
            this.txbLeftStr.Location = new System.Drawing.Point(132, 379);
            this.txbLeftStr.Name = "txbLeftStr";
            this.txbLeftStr.Size = new System.Drawing.Size(279, 26);
            this.txbLeftStr.TabIndex = 18;
            // 
            // btnUpper
            // 
            this.btnUpper.Location = new System.Drawing.Point(484, 72);
            this.btnUpper.Name = "btnUpper";
            this.btnUpper.Size = new System.Drawing.Size(126, 39);
            this.btnUpper.TabIndex = 19;
            this.btnUpper.Text = "Upper";
            this.btnUpper.UseVisualStyleBackColor = true;
            this.btnUpper.Click += new System.EventHandler(this.BtnUpper_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(626, 72);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(126, 39);
            this.btnCopy.TabIndex = 20;
            this.btnCopy.Text = "Copy";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.BtnCopy_Click);
            // 
            // btnLeftPadding
            // 
            this.btnLeftPadding.Location = new System.Drawing.Point(772, 72);
            this.btnLeftPadding.Name = "btnLeftPadding";
            this.btnLeftPadding.Size = new System.Drawing.Size(126, 39);
            this.btnLeftPadding.TabIndex = 21;
            this.btnLeftPadding.Text = "Left padding";
            this.btnLeftPadding.UseVisualStyleBackColor = true;
            this.btnLeftPadding.Click += new System.EventHandler(this.BtnLeftPadding_Click);
            // 
            // btnPaddingSpace
            // 
            this.btnPaddingSpace.Location = new System.Drawing.Point(772, 117);
            this.btnPaddingSpace.Name = "btnPaddingSpace";
            this.btnPaddingSpace.Size = new System.Drawing.Size(126, 39);
            this.btnPaddingSpace.TabIndex = 24;
            this.btnPaddingSpace.Text = "Padding space";
            this.btnPaddingSpace.UseVisualStyleBackColor = true;
            this.btnPaddingSpace.Click += new System.EventHandler(this.BtnPaddingSpace_Click);
            // 
            // btnConcat
            // 
            this.btnConcat.Location = new System.Drawing.Point(626, 117);
            this.btnConcat.Name = "btnConcat";
            this.btnConcat.Size = new System.Drawing.Size(126, 39);
            this.btnConcat.TabIndex = 23;
            this.btnConcat.Text = "Concat";
            this.btnConcat.UseVisualStyleBackColor = true;
            this.btnConcat.Click += new System.EventHandler(this.BtnConcat_Click);
            // 
            // btnLower
            // 
            this.btnLower.Location = new System.Drawing.Point(484, 117);
            this.btnLower.Name = "btnLower";
            this.btnLower.Size = new System.Drawing.Size(126, 39);
            this.btnLower.TabIndex = 22;
            this.btnLower.Text = "Lower";
            this.btnLower.UseVisualStyleBackColor = true;
            this.btnLower.Click += new System.EventHandler(this.BtnLower_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(772, 162);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(126, 39);
            this.btnClear.TabIndex = 27;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // btnCompare
            // 
            this.btnCompare.Location = new System.Drawing.Point(626, 162);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(126, 39);
            this.btnCompare.TabIndex = 26;
            this.btnCompare.Text = "Compare";
            this.btnCompare.UseVisualStyleBackColor = true;
            this.btnCompare.Click += new System.EventHandler(this.BtnCompare_Click);
            // 
            // btnCount
            // 
            this.btnCount.Location = new System.Drawing.Point(484, 162);
            this.btnCount.Name = "btnCount";
            this.btnCount.Size = new System.Drawing.Size(126, 39);
            this.btnCount.TabIndex = 25;
            this.btnCount.Text = "Count";
            this.btnCount.UseVisualStyleBackColor = true;
            this.btnCount.Click += new System.EventHandler(this.BtnCount_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(991, 446);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCompare);
            this.Controls.Add(this.btnCount);
            this.Controls.Add(this.btnPaddingSpace);
            this.Controls.Add(this.btnConcat);
            this.Controls.Add(this.btnLower);
            this.Controls.Add(this.btnLeftPadding);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.btnUpper);
            this.Controls.Add(this.txbLeftStr);
            this.Controls.Add(this.txbPadding);
            this.Controls.Add(this.txbCompare);
            this.Controls.Add(this.txbConcat);
            this.Controls.Add(this.txbCopy);
            this.Controls.Add(this.txbCount);
            this.Controls.Add(this.txbLower);
            this.Controls.Add(this.txbUpper);
            this.Controls.Add(this.txbInput);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txbInput;
        private System.Windows.Forms.TextBox txbUpper;
        private System.Windows.Forms.TextBox txbLower;
        private System.Windows.Forms.TextBox txbCount;
        private System.Windows.Forms.TextBox txbCopy;
        private System.Windows.Forms.TextBox txbConcat;
        private System.Windows.Forms.TextBox txbCompare;
        private System.Windows.Forms.TextBox txbPadding;
        private System.Windows.Forms.TextBox txbLeftStr;
        private System.Windows.Forms.Button btnUpper;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnLeftPadding;
        private System.Windows.Forms.Button btnPaddingSpace;
        private System.Windows.Forms.Button btnConcat;
        private System.Windows.Forms.Button btnLower;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.Button btnCount;
    }
}

